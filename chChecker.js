function channelIs(patterns, numCh) {
    var name = trackBank.getTrack(numCh).name().get()

    for (idx in patterns) {
        if (name.toLowerCase().indexOf(patterns[idx]) !== -1) {
            return true
        }
    }
    return false
}

function channelIsDrum(numCh) {
    return channelIs(['drum', 'beat'], numCh)
}

function channelIsVox(numCh) {
    return channelIs(['vox','voz'], numCh)

}

function channelIsGuitar(numCh){
    return channelIs(['guitarra', 'guit', 'viola'], numCh)
}

function channelIsInput(numCh){
    return channelIsVox(numCh) || channelIsGuitar(numCh) || channelIs(['input', 'in', 'rec'], numCh)
}

function channelIsTransition(numCh){
  var name = trackBank.getTrack(numCh).name().get()
  return  name == 'transiciones' || name == 'trans'
}

function channelBelongsToUser(numCh){
    println('userName: ' + userName)
    var name = trackBank.getTrack(numCh).name().get()
    println('channelName: ' + name)
    println('IS? : ' + name.indexOf(userName))
    return name.indexOf(userName) !== -1
}
