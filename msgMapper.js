load('./controllerMappings.js')

function ccToChannel (cc) {
  var val = map(cc, CONTROLLER.PADS.CC.BANK_1[0], CONTROLLER.PADS.CC.BANK_1[1], 0, 7)
  return invertOrder(val)
}

function channelToCC (ch) {
  var val = map(ch, 0, 7, CONTROLLER.PADS.CC.BANK_1[0], CONTROLLER.PADS.CC.BANK_1[1])
  return val
}

function ccToControl (cc) {
  return map(cc, CONTROLLER.KNOBS[0], CONTROLLER.KNOBS[1], 0, 7)
}

function ccToClipSlot (cc) {
  var val = map(cc, CONTROLLER.PADS.CC.BANK_2[0], CONTROLLER.PADS.CC.BANK_2[1], 0, 7)
  return invertOrder(val)
}

function pgToScene (pg) {
  return invertOrder(pg)
}

function map (x, in_min, in_max, out_min, out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
}

function invertOrder (val) {
  if (val >= 4) {
    val -= 4
  } else {
    val += 4
  }
  println(val)
  return val
}
