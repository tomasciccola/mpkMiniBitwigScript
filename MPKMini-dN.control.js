loadAPI(6)

host.defineController(
  'Akai',
  'MPKmini-devNull',
  '1.0',
  '75571af0-6f02-11e7-9598-0800200c9a66')

host.defineMidiPorts(1, 1)
host.addDeviceNameBasedDiscoveryPair(['MPK mini'], ['MPK mini'])

load('./msgChecker.js')
load('./msgMapper.js')

var DEBUG = false

var N_CONTROLS = 8
var N_CHANNELS = 8
var N_SENDS = 4
var N_SCENES = 16

var SHIFT = false
var atScene = 0
var isSceneStopped = true

var USER = {}
var remoteControls = []

function init () {
  // println(AutoDetectionMidiPortNames)
  host.getMidiInPort(0).setMidiCallback(onMidi)
  var mpk = host.getMidiInPort(0).createNoteInput('MPKmini', '??????')

  mpk.setShouldConsumeEvents(false)

  // cosas bien generales del programa
  // (desde copiar y pegar hasta crear tracks y activar el engine)
  app = host.createApplication()

  // para moverme entre devices
  // cursorDevice = cursorTrack.createCursorDevice("Primary")

  // banco de 8 track, 4 sends y 8 escenas
  trackBank = host.createTrackBank(N_CHANNELS, N_SENDS, N_SCENES)
  // cursor que permite moverme entre tracks
  cursorTrack = host.createCursorTrack(
    'MPK_CURSOR_TRACK',
    'Cursor Track',
    0,
    N_CHANNELS,
    true)

  sceneBank = host.createSceneBank(N_SCENES)

  trackBank.followCursorTrack(cursorTrack)
  trackBank.cursorIndex().markInterested()

  // cosas del tipo play stop, click, loop, etc
  // transport = host.createTransportSection();
  // Preferencias
  // userPreferences = host.getPreferences()

  /** **************** REMOTE CONTROLS SETUP *********************/

  for (let i = 0; i < N_CHANNELS; i++) {
    let tr = trackBank.getTrack(i)

    for (let j = 0; j < N_SENDS; j++) {
      tr.getSend(j).setIndication(true)
    }

    tr.name().markInterested()
    let cursorDevice = tr.createCursorDevice(i)

    let remoteControl = cursorDevice.createCursorRemoteControlsPage(N_CONTROLS)

    remoteControl.getName().markInterested()
    remoteControl.selectedPageIndex().markInterested()

    for (let j = 0; j < N_CONTROLS; j++) {
      let p = remoteControl.getParameter(j).getAmount()
      p.setIndication(true)
    }
    remoteControls.push(remoteControl)
  }

  /**************************************************************/
  // host.getPortNames()
  // app.AutoDetectionMidiPortNamesList.getPortNames()
  host.showPopupNotification('cargado :)')
  println('holi from la consola')
}

/* ---------MAIN SHIT--------------- */

function onMidi (status, d1, d2) {
  if (DEBUG) {
    pPrint(status, d1, d2)
  }

  if (isKnobInput(status, d1)) {
    let controlNum = ccToControl(d1)
    let controlVal = d2

    if (!SHIFT) {
      // REMOTE CONTROLS
      USER.controls = remoteControls[getActiveChannel()]
      USER.controls
        .getParameter(controlNum)
        .getAmount()
        .value()
        .set(controlVal, 128)
    } else {
      if (controlNum < N_SENDS) {
        // SENDS
        trackBank
          .getTrack(getActiveChannel())
          .getSend(controlNum)
          .set(controlVal, 128)
      }
    }
  }

  if (isXJoystick(status, d1)) {
    remoteControlPageManage(d2)
  }

  if (isCCPadBank1_On(status, d1, d2)) {
    if (d1 == 10) {
      SHIFT = true
    }
  }

  if (isCCPadBank1_Off(status, d1, d2)) {
    if (d1 == 10) {
      SHIFT = false
    }
  }

  if (isPG(status)) {
    // selectScene(pgToScene(d1))
  }
}

/* --------------------------------- */

function remoteControlPageManage (val) {
  if (USER.controls) {
    if (val == 0) {
      USER.controls.selectPreviousPage(true)

      host.scheduleTask(function () {
        host.showPopupNotification(USER.name + ': ' + USER.controls.getName().get())
      }, 100)
    } else if (val == 127) {
      USER.controls.selectNextPage(true)

      host.scheduleTask(function () {
        host.showPopupNotification(USER.name + ': ' + USER.controls.getName().get())
      }, 100)
    }
  }
}

function selectScene (sceneNum) {
  if (atScene == sceneNum && !isSceneStopped) {
    sceneBank.stop()
    isSceneStopped = true
  } else {
    sceneBank.launchScene(sceneNum)
    atScene = sceneNum
    isSceneStopped = false
  }
}

/* ---------------EN MKII NO FUNCIONA MANDAR MIDI OUT---------------- */

function testOut () {
  host.scheduleTask(run(10), 1000)

  function run (val) {
    return function () {
      if (val < 18) {
        println(val)
        midiOut.sendMidi(176, val, 127)
        host.scheduleTask(run(++val), 1000)
      }
    }
  }
}

/* ----------------------UTILS--------------------------------------- */
function getTrackNames () {
  for (let i = 0; i < N_CHANNELS; i++) {
    let name = trackBank.getTrack(i).name().get()
    println('name: ' + name)
  }
}

function getActiveChannel () {
  // println(trackBank.cursorIndex().get())
  return trackBank.cursorIndex().get()
}

function pPrint (s, d1, d2) {
  println('nuevoMensaje: ')
  println('s: ' + s + '|' + 'd1: ' + d1 + '|' + 'd2: ' + d2)
}
