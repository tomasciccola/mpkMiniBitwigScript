load('controllerMappings.js')
// GENERALES
function isNoteOn (status) {
  return status == 144
}

function isNoteOf (status) {
  return status == 128
}

function isCC (status) {
  return status == 176
}

function isPG (status) {
  return status == 192
}

function isPitchBend (status) {
  return status == 224
}

// PARTICULARES

// CONTROL CHANGE
function isKnobInput (status, cc) {
  return isCC(status) &&
        (cc >= CONTROLLER.KNOBS[0] &&
         cc <= CONTROLLER.KNOBS[1])
}

function isCCPadBank1 (status, cc) {
  return isCC(status) &&
        (cc >= CONTROLLER.PADS.CC.BANK_1[0] &&
         cc <= CONTROLLER.PADS.CC.BANK_1[1])
}

function isCCPadBank2 (status, cc) {
  return isCC(status) &&
        (cc >= CONTROLLER.PADS.CC.BANK_2[0] &&
         cc <= CONTROLLER.PADS.CC.BANK_2[1])
}

function isCCPadBank1_On (status, cc, vel) {
  return isCCPadBank1(status, cc) && vel != 0
}

function isCCPadBank1_Off (status, cc, vel) {
  return isCCPadBank1(status, cc) && vel == 0
}

function isCCPadBank2_On (status, cc, vel) {
  return isCCPadBank2(status, cc) && vel != 0
}

function isCCPadBank2_Off (status, cc, vel) {
  return isCCPadBank2(status, cc) && vel == 0
}

// PROGRAM CHANGE

function isPGPadBank1 (status, pg) {
  return isPG(status) &&
        (pg >= CONTROLLER.PADS.PG.BANK_1[0] &&
         pg <= CONTROLLER.PADS.PG.BANK_1[1])
}

function isPGPadBank2 (status, pg) {
  return isPG(status) &&
        (pg >= CONTROLLER.PADS.PG.BANK_2[0] &&
         pg <= CONTROLLER.PADS.PG.BANK_2[1])
}

// JOYSTICK

function isXJoystick (status, cc) {
  return isCC(status) && cc == 0
}
