var CONTROLLER = {
    PADS: {
        CC: {
            BANK_1: [10, 17],
            BANK_2: [18, 25]
        },
        PG: {
            BANK_1: [0, 7],
            BANK_2: [8, 15]
        }
    },
    KNOBS: [2, 9],
    JOYSTICK: {
        X: 1,
        Y: "PITCH_BEND"
    }
}
